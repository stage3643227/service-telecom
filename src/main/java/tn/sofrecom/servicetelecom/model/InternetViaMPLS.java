package tn.sofrecom.servicetelecom.model;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Data
@Table(name = "InternetViaMPLS")
public class InternetViaMPLS extends ServiceTelecom  {
    @Column(name = "state")
    private String state;
    @Column(name = "id_site")
    private Long idSite ;
}
