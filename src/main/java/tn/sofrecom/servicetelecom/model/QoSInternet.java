package tn.sofrecom.servicetelecom.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Data
@Table(name = "QoSInternet")
public class QoSInternet extends ServiceTelecom {
    @Column(name = "band_width")
    private Long bandWidth ;
    @Column(name = "id_site")
    private Long idSite ;


}
