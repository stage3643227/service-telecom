package tn.sofrecom.servicetelecom.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Data
@Table(name = "L3VPN")
public class L3VPN extends ServiceTelecom {

   @Column(name = "state")
   private String state;
   @JsonIgnore
   @OneToMany(mappedBy = "id.idService")
   private Set<Site_L3Vpn> sites;

}
