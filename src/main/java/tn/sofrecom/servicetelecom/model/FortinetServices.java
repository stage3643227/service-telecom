package tn.sofrecom.servicetelecom.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;


@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Data
@Table(name = "FortinetServices")
public class FortinetServices extends ServiceTelecom {
    @Column(name = "service_name")
    private String serviceName;
    @Column(name = "license_expiration")
    private LocalDateTime licenseExpiration ;
    @Column(name = "url")
    private String url ;

    @JsonManagedReference
    @OneToOne(mappedBy = "serviceType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    private UserAccount userAccount;
    @JsonIgnore
    @OneToMany(mappedBy = "id.idService")
    private Set<Site_Fortinet> sites;
}
