package tn.sofrecom.servicetelecom.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SiteFortinetId implements Serializable {
    private Long idSite ;
    private  Long idService;
}
