package tn.sofrecom.servicetelecom.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Site_L3Vpn {
    @EmbeddedId
    private SiteL3VpnId id;
}
