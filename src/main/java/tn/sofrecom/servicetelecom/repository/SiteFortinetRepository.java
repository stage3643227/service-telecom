package tn.sofrecom.servicetelecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicetelecom.model.FortinetServices;
import tn.sofrecom.servicetelecom.model.SiteFortinetId;
import tn.sofrecom.servicetelecom.model.Site_Fortinet;

import java.util.List;

@Repository
public interface SiteFortinetRepository extends JpaRepository<Site_Fortinet, SiteFortinetId> {
    @Query("SELECT s FROM Site_Fortinet s WHERE s.id.idSite = ?1")
    List<Site_Fortinet> findByIdSite(Long idSite);

    @Query("SELECT s FROM FortinetServices s WHERE s.idService = ?1")
    FortinetServices findServiceByIdService(Long idService);
}
