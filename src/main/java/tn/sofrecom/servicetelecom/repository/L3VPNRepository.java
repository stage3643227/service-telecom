package tn.sofrecom.servicetelecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicetelecom.model.L3VPN;

import java.util.Optional;

@Repository
public interface L3VPNRepository extends JpaRepository<L3VPN,Long> {
    Optional<L3VPN> findByTypeService(String typeService);
}
