package tn.sofrecom.servicetelecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicetelecom.model.ServiceTelecom;

import java.util.List;

@Repository
public interface ServiceTelecomRepository extends JpaRepository<ServiceTelecom, Long> {
}
