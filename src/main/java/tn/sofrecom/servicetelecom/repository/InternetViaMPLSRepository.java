package tn.sofrecom.servicetelecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicetelecom.model.InternetViaMPLS;


import java.util.Optional;

@Repository
public interface InternetViaMPLSRepository extends JpaRepository<InternetViaMPLS,Long> {
    Optional<InternetViaMPLS> findByTypeService(String typeService);
    Optional<InternetViaMPLS> findInternetViaMPLSByIdSite(Long idSite);
}
