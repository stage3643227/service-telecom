package tn.sofrecom.servicetelecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicetelecom.model.QoSInternet;

import java.util.Optional;

@Repository
public interface QosSInternetRepository extends JpaRepository<QoSInternet,Long> {
    Optional<QoSInternet> findByTypeService(String typeService);
    Optional<QoSInternet> findQoSInternetByIdSite(Long idSite);
}
