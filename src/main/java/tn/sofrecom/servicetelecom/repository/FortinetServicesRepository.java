package tn.sofrecom.servicetelecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicetelecom.model.FortinetServices;
@Repository
public interface FortinetServicesRepository extends JpaRepository<FortinetServices,Long> {
    FortinetServices findByServiceName(String serviceName);
}
