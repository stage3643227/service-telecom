package tn.sofrecom.servicetelecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicetelecom.model.UserAccount;
@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
}
