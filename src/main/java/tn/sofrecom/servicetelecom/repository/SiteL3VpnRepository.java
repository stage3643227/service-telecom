package tn.sofrecom.servicetelecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicetelecom.model.L3VPN;
import tn.sofrecom.servicetelecom.model.SiteL3VpnId;
import tn.sofrecom.servicetelecom.model.Site_L3Vpn;

import java.util.List;

@Repository
public interface SiteL3VpnRepository extends JpaRepository<Site_L3Vpn , SiteL3VpnId> {
    @Query("SELECT s FROM Site_L3Vpn s WHERE s.id.idSite = ?1")
    List<Site_L3Vpn> findByIdSite(Long idSite);

    @Query("SELECT s FROM L3VPN s WHERE s.idService = ?1")
    L3VPN findServiceByIdService(Long idService);
}
