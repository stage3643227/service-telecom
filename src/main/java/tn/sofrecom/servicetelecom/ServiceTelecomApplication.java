package tn.sofrecom.servicetelecom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceTelecomApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceTelecomApplication.class, args);
    }

}
