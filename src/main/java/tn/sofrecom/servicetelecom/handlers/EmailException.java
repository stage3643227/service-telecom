package tn.sofrecom.servicetelecom.handlers;

public class EmailException extends RuntimeException{
    public EmailException(String message){
        super(message);
    }
}
