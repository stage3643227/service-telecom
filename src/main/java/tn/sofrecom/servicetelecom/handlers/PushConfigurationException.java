package tn.sofrecom.servicetelecom.handlers;

public class PushConfigurationException extends RuntimeException {
    public PushConfigurationException(String message) {
        super(message);
    }
}
