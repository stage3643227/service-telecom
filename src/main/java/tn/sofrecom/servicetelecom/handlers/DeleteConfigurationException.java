package tn.sofrecom.servicetelecom.handlers;

public class DeleteConfigurationException extends RuntimeException{
    public DeleteConfigurationException(String message){
        super(message);
    }
}
