package tn.sofrecom.servicetelecom.handlers;

public class SiteRefServiceBussinessException  extends RuntimeException{
    public SiteRefServiceBussinessException(String message) {
        super(message);
    }
}
