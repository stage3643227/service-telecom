package tn.sofrecom.servicetelecom.handlers;

public class SiteRefNotFoundException  extends RuntimeException{
    public SiteRefNotFoundException(String message) {
        super(message);
    }
}
