package tn.sofrecom.servicetelecom.handlers;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import tn.sofrecom.servicetelecom.dto.APIResponse;
import tn.sofrecom.servicetelecom.dto.ErrorDTO;

import java.util.Collections;

@RestControllerAdvice
public class TestConfigurationHandler {
    public static final String FAILED = "FAILED";
    @ExceptionHandler(PushConfigurationException.class)
    public APIResponse<?> handleTestConfigurationException(TestConfigurationException exception) {
        APIResponse<?> serviceResponse = new APIResponse<>();
        serviceResponse.setStatus(FAILED);
        serviceResponse.setErrors(Collections.singletonList(new ErrorDTO("", exception.getMessage())));
        return serviceResponse;
    }
}
