package tn.sofrecom.servicetelecom.handlers;

public class TestConfigurationException extends RuntimeException{
    public TestConfigurationException(String message) {
        super(message);
    }
}
