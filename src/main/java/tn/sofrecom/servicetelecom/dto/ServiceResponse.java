package tn.sofrecom.servicetelecom.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tn.sofrecom.servicetelecom.dto.site.SiteDTO;
import tn.sofrecom.servicetelecom.model.ServiceTelecom;

import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ServiceResponse {
    private ServiceTelecom service;
    private Set<SiteDTO> siteDTOS;

}
