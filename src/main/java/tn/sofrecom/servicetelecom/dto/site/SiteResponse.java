package tn.sofrecom.servicetelecom.dto.site;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SiteResponse {
    private long idSite;

    private String city;

    public SiteResponse(long idSite) {
        this.idSite = idSite ;
    }
}
