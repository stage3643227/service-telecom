package tn.sofrecom.servicetelecom.dto.site;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tn.sofrecom.servicetelecom.model.FortinetServices;
import tn.sofrecom.servicetelecom.model.InternetViaMPLS;
import tn.sofrecom.servicetelecom.model.L3VPN;
import tn.sofrecom.servicetelecom.model.QoSInternet;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SiteRefResponseDTO {
    private Long idSiteRef;
    private List<L3VPN> l3vpns;
    private List<QoSInternet> qosInternets;
    private List<FortinetServices> fortinetServices  ;
    private List<InternetViaMPLS> internetViaMPLS;
}
