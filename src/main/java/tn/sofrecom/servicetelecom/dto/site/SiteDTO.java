package tn.sofrecom.servicetelecom.dto.site;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SiteDTO {
    private long idSite;
    private double latitude;
    private double longitude;

    private String country;

    private String city;

    private String nameTopology;

    private String nameCompany;




}