package tn.sofrecom.servicetelecom.dto.site;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SiteRefRequestDTO {
    private Long idSiteRef;
}
