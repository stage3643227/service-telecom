package tn.sofrecom.servicetelecom.dto.internetViaMpls;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class InternetViaMPLSRequestDTO {
    List<String> city = new ArrayList<>() ;
    private List<Long> siteIds  = new ArrayList<>();
    private Long bandWidth ;
}
