package tn.sofrecom.servicetelecom.dto.internetViaMpls;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class InternetResponse {
    private Long idServiceInternet;

}
