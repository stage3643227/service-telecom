package tn.sofrecom.servicetelecom.dto.internetViaMpls;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class InternetViaMplsRequest {
    private List<Long> siteIds;
    private  Long idService ;
}
