package tn.sofrecom.servicetelecom.dto.l3VPN;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class L3VpnRequestPushConfigurationDTO {
    List<String> city = new ArrayList<>() ;
    private List<Long> siteIds  = new ArrayList<>();
}
