package tn.sofrecom.servicetelecom.dto.l3VPN;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class L3VpnRequest {
 private List<Long> siteIds;
 private  Long idService ;
}
