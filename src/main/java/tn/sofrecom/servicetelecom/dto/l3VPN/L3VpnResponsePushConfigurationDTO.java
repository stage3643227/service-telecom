package tn.sofrecom.servicetelecom.dto.l3VPN;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class L3VpnResponsePushConfigurationDTO {
    String status ;
    Map<String, String> devices ;


}
