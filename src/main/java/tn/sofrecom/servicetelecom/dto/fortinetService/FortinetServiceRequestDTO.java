package tn.sofrecom.servicetelecom.dto.fortinetService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class FortinetServiceRequestDTO {
    @NotBlank(message = "service Name shouldn't be NULL OR EMPTY")
    private String serviceName;
    private LocalDateTime licenseExpiration ;
}
