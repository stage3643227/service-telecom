package tn.sofrecom.servicetelecom.dto.fortinetService;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FortinetServiceResponseDTO {
    private String serviceName;
    private LocalDateTime licenseExpiration ;
}
