package tn.sofrecom.servicetelecom.dto.useraccount;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tn.sofrecom.servicetelecom.model.FortinetServices;
import tn.sofrecom.servicetelecom.model.UserAccount;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CreateUserRequest {
    private UserAccount userAccount;
    private FortinetServices fortinetService;
}
