package tn.sofrecom.servicetelecom.dto.qosInternet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class QosInternetPushConfigRequestDTO {
    List<String> city = new ArrayList<>() ;
    private List<Long> siteIds  = new ArrayList<>();
    private Long bandWidth ;
}
