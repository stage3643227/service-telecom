package tn.sofrecom.servicetelecom.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ServiceRequestDTO {
   private String serviceType ;
   private List<Long> siteIds;
   private String serviceName;
   private Long idUser ;
   private Long bandwidth ;
}
