package tn.sofrecom.servicetelecom.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserResponse {
    private Long idUser;
    private String firstName;
    private String lastName;
    private String email;
    private Long idCompany;
}
