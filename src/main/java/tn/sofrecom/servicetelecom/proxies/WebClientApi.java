package tn.sofrecom.servicetelecom.proxies;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;
import tn.sofrecom.servicetelecom.dto.APIResponse;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetViaMPLSRequestDTO;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetViaMPLSResponseDTO;
import tn.sofrecom.servicetelecom.dto.l3VPN.L3VpnRequestPushConfigurationDTO;
import tn.sofrecom.servicetelecom.dto.l3VPN.L3VpnResponsePushConfigurationDTO;
import tn.sofrecom.servicetelecom.dto.qosInternet.QosInternetPushConfigRequestDTO;
import tn.sofrecom.servicetelecom.dto.qosInternet.QosInternetPushConfigResponseDTO;
import tn.sofrecom.servicetelecom.dto.site.SiteDTO;
import tn.sofrecom.servicetelecom.dto.site.SiteResponse;
import tn.sofrecom.servicetelecom.dto.user.UserResponse;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Slf4j
@Component
public class WebClientApi {
    private final WebClient webClient;

    @Value("${api.sites.base-url}")
    private String sitesBaseUrl;

    @Value("${api.sites.get-specific-site}")
    private String getSpecificSitePath;

    @Value("${api.sites.get-sites-by-id-company}")
    private String getSitesByIdCompanyPath;

    @Value("${api.users.base-url}")
    private String usersBaseUrl;

    @Value("${api.l3vpn.base-url}")
    private String configBaseUrl;

    @Value("${api.l3vpn.push}")
    private String pushPath;

    @Value("${api.l3vpn.test}")
    private String testPath;

    @Value("${api.qosInternet.pushQos}")
    private String pushQos;

    @Value("${api.l3vpn.rollback}")
    private String rollbackPath;

    @Value("${api.internet.pushNetwork}")
    private String pushInternet;

    @Value("${api.internet.testNetwork}")
    private String testNetwork;

    @Value("${api.internet.rollbackNetwork}")
    private String rollbackNetwork;

    private static final String COUNTRIES_KEY = "countries";
    private static final String SITE_IDS_KEY = "siteIds";
    private static final String STATUS = "status";
    private static final String RETURN_CODE = "return_code";
    private static final String DEVICE = "devices";
    private static final String BANDWIDTH = "bandwidth";
    public WebClientApi(WebClient webClient) {
        this.webClient = webClient;
    }
    public List<SiteResponse> fetchSiteResponses(List<Long> siteIds) {
        Mono<APIResponse<List<SiteResponse>>> siteResponsesMono = webClient.post()
                .uri(sitesBaseUrl + getSpecificSitePath)
                .bodyValue(siteIds)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<APIResponse<List<SiteResponse>>>() {
                })
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors de la récupération des sites : " + e.getMessage());
                    return Mono.empty();
                });

        APIResponse<List<SiteResponse>> apiResponse = siteResponsesMono.block();
        return apiResponse != null ? apiResponse.getResults() : Collections.emptyList();
    }
    public SiteDTO fetchSiteResponse(Long siteId) {
        Mono<APIResponse> siteResponseMono = webClient.get()
                .uri(sitesBaseUrl+"/" + siteId)
                .retrieve()
                .bodyToMono(APIResponse.class)
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors de la récupération du site : " + e.getMessage());
                    return Mono.empty();
                });

        APIResponse apiResponse = siteResponseMono.block();
        if (apiResponse != null) {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.convertValue(apiResponse.getResults(), SiteDTO.class);
        } else {
            return null;
        }
    }


    public List<SiteResponse> fetchSiteByIdCompanyResponses(Long idCompany) {
        Mono<APIResponse<List<SiteResponse>>> siteResponsesMono = webClient.get()
                .uri(sitesBaseUrl + getSitesByIdCompanyPath+ "/" +idCompany)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<APIResponse<List<SiteResponse>>>() {
                })
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors de la récupération des sites par idCompany : " + e.getMessage());
                    return Mono.empty();
                });

        APIResponse<List<SiteResponse>> apiResponse = siteResponsesMono.block();
        return apiResponse != null ? apiResponse.getResults() : Collections.emptyList();
    }

    public UserResponse fetchUserResponseByID(Long idUser) {
        Mono<APIResponse<UserResponse>> userResponseMono = webClient.get()
                .uri(usersBaseUrl + "/" + idUser)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<APIResponse<UserResponse>>() {
                })
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors de la récupération de l'utilisateur par ID : " + e.getMessage());
                    return Mono.empty();
                });

        APIResponse<UserResponse> apiResponse = userResponseMono.block();
        return apiResponse != null ? apiResponse.getResults() : null;
    }




    public L3VpnResponsePushConfigurationDTO pushConfiguration(L3VpnRequestPushConfigurationDTO l3VpnRequestPushConfigurationDTO) {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put(COUNTRIES_KEY, l3VpnRequestPushConfigurationDTO.getCity());
        requestBody.put(SITE_IDS_KEY, l3VpnRequestPushConfigurationDTO.getSiteIds());

        return webClient.post()
                .uri(configBaseUrl+pushPath)
                .bodyValue(requestBody)
                .retrieve()
                .bodyToMono(Map.class)
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors de la configuration de L3VPN : " + e.getMessage());
                    return Mono.empty();
                })
                .map(result -> {
                    if (result != null && result.get(STATUS) != null) {
                        String status = (String) result.get(STATUS);
                        Map<String, String> devices = (Map<String, String>) result.get(DEVICE);
                        return new L3VpnResponsePushConfigurationDTO(status, devices);
                    } else {
                        return null;
                    }
                })
                .block();
    }



    public QosInternetPushConfigResponseDTO pushConfigurationQos(QosInternetPushConfigRequestDTO qosInternetPushConfigRequestDTO) {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put(COUNTRIES_KEY, qosInternetPushConfigRequestDTO.getCity());
        requestBody.put(SITE_IDS_KEY, qosInternetPushConfigRequestDTO.getSiteIds());
        requestBody.put(BANDWIDTH, qosInternetPushConfigRequestDTO.getBandWidth());
        log.info("{}",requestBody);
        return webClient.post()
                .uri(configBaseUrl+pushQos)
                .bodyValue(requestBody)
                .retrieve()
                .bodyToMono(Map.class)
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors de la configuration de QOS : " + e.getMessage());
                    return Mono.empty();
                })
                .map(result -> {
                    if (result != null && result.get(STATUS) != null) {
                        String status = (String) result.get(STATUS);
                        Map<String, String> devices = (Map<String, String>) result.get(DEVICE);
                        return new QosInternetPushConfigResponseDTO(status, devices);
                    } else {
                        return null;
                    }
                })
                .block();
    }

    public InternetViaMPLSResponseDTO pushInternetViaMPLSConfiguration(InternetViaMPLSRequestDTO internetViaMPLSRequestDTO) {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put(COUNTRIES_KEY, internetViaMPLSRequestDTO.getCity());
        requestBody.put(SITE_IDS_KEY, internetViaMPLSRequestDTO.getSiteIds());
        requestBody.put(BANDWIDTH, internetViaMPLSRequestDTO.getBandWidth());

        return webClient.post()
                .uri(configBaseUrl+pushInternet)
                .bodyValue(requestBody)
                .retrieve()
                .bodyToMono(Map.class)
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors de la configuration de internet via mpls : " + e.getMessage());
                    return Mono.empty();
                })
                .map(result -> {
                    if (result != null && result.get(STATUS) != null) {
                        String status = (String) result.get(STATUS);
                        Map<String, String> devices = (Map<String, String>) result.get(DEVICE);
                        return new InternetViaMPLSResponseDTO(status, devices);
                    } else {
                        return null;
                    }
                })
                .block();
    }
    public Integer testConfiguration(L3VpnRequestPushConfigurationDTO l3VpnRequestPushConfigurationDTO) {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put(COUNTRIES_KEY, l3VpnRequestPushConfigurationDTO.getCity());
        requestBody.put(SITE_IDS_KEY, l3VpnRequestPushConfigurationDTO.getSiteIds());

        return webClient.post()
                .uri(configBaseUrl+testPath)
                .bodyValue(requestBody)
                .retrieve()
                .bodyToMono(Map.class)
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors du test de configuration de L3VPN : " + e.getMessage());
                    return Mono.empty();
                })
                .map(result -> {
                    if (result != null && result.get(RETURN_CODE) != null) {
                        return (Integer) result.get(RETURN_CODE);
                    } else {
                        return null;
                    }
                })
                .block();
    }

    public Integer testInternetConfiguration(InternetViaMPLSRequestDTO internetViaMPLSRequestDTO) {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put(COUNTRIES_KEY, internetViaMPLSRequestDTO.getCity());
        requestBody.put(SITE_IDS_KEY, internetViaMPLSRequestDTO.getSiteIds());

        return webClient.post()
                .uri(configBaseUrl+testNetwork)
                .bodyValue(requestBody)
                .retrieve()
                .bodyToMono(Map.class)
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors du test de configuration de internet : " + e.getMessage());
                    return Mono.empty();
                })
                .map(result -> {
                    if (result != null && result.get(RETURN_CODE) != null) {
                        return (Integer) result.get(RETURN_CODE);
                    } else {
                        return null;
                    }
                })
                .block();
    }

    public InternetViaMPLSResponseDTO rollBackInternetConfiguration(InternetViaMPLSRequestDTO internetViaMPLSRequestDTO) {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put(COUNTRIES_KEY, internetViaMPLSRequestDTO.getCity());
        requestBody.put(SITE_IDS_KEY, internetViaMPLSRequestDTO.getSiteIds());

        return webClient.post()
                .uri(configBaseUrl+rollbackNetwork)
                .bodyValue(requestBody)
                .retrieve()
                .bodyToMono(Map.class)
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors du rollback de la configuration de internet : " + e.getMessage());
                    return Mono.empty();
                })
                .map(result -> {
                    if (result != null && result.get(STATUS) != null) {
                        String status = (String) result.get(STATUS);
                        Map<String, String> devices = (Map<String, String>) result.get("devices");
                        return new InternetViaMPLSResponseDTO(status, devices);
                    } else {
                        return null;
                    }
                })
                .block();
    }

    public L3VpnResponsePushConfigurationDTO rollBackConfiguration(L3VpnRequestPushConfigurationDTO l3VpnRequestPushConfigurationDTO) {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put(COUNTRIES_KEY, l3VpnRequestPushConfigurationDTO.getCity());
        requestBody.put(SITE_IDS_KEY, l3VpnRequestPushConfigurationDTO.getSiteIds());

        return webClient.post()
                .uri(configBaseUrl+rollbackPath)
                .bodyValue(requestBody)
                .retrieve()
                .bodyToMono(Map.class)
                .onErrorResume(WebClientResponseException.class, e -> {
                    log.error("Erreur lors du rollback de la configuration de L3VPN : " + e.getMessage());
                    return Mono.empty();
                })
                .map(result -> {
                    if (result != null && result.get(STATUS) != null) {
                        String status = (String) result.get(STATUS);
                        Map<String, String> devices = (Map<String, String>) result.get("devices");
                        return new L3VpnResponsePushConfigurationDTO(status, devices);
                    } else {
                        return null;
                    }
                })
                .block();
    }


}
