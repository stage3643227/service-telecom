package tn.sofrecom.servicetelecom.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.sofrecom.servicetelecom.dto.useraccount.CreateUserRequest;
import tn.sofrecom.servicetelecom.model.FortinetServices;
import tn.sofrecom.servicetelecom.model.UserAccount;
import tn.sofrecom.servicetelecom.repository.FortinetServicesRepository;
import tn.sofrecom.servicetelecom.repository.UserAccountRepository;

@Service
@Slf4j
@Transactional
public class FortinetService {


    private final FortinetServicesRepository fortinetServicesRepository;


    private final UserAccountRepository userAccountRepository;

    public FortinetService(FortinetServicesRepository fortinetServicesRepository, UserAccountRepository userAccountRepository) {
        this.fortinetServicesRepository = fortinetServicesRepository;
        this.userAccountRepository = userAccountRepository;
    }

    public FortinetServices createFortinetServiceWithUser(CreateUserRequest request) {

        FortinetServices fortinetService = new FortinetServices();
        fortinetService.setServiceName(request.getFortinetService().getServiceName());
        fortinetService.setLicenseExpiration(request.getFortinetService().getLicenseExpiration());
        fortinetService.setUrl(request.getFortinetService().getUrl());
        fortinetService.setTypeService("FortinetServices");
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername(request.getUserAccount().getUsername());
        userAccount.setPassword(request.getUserAccount().getPassword());

        fortinetService.setUserAccount(userAccount);
        userAccount.setServiceType(fortinetService);


        FortinetServices savedFortinetService = fortinetServicesRepository.save(fortinetService);
        userAccountRepository.save(userAccount);

        return savedFortinetService;
    }
}
