package tn.sofrecom.servicetelecom.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.sofrecom.servicetelecom.dto.ServiceResponse;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetResponse;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetViaMPLSRequestDTO;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetViaMPLSResponseDTO;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetViaMplsRequest;
import tn.sofrecom.servicetelecom.dto.l3VPN.L3VpnRequest;
import tn.sofrecom.servicetelecom.dto.l3VPN.L3VpnRequestPushConfigurationDTO;
import tn.sofrecom.servicetelecom.dto.l3VPN.L3VpnResponsePushConfigurationDTO;
import tn.sofrecom.servicetelecom.dto.site.SiteDTO;
import tn.sofrecom.servicetelecom.dto.site.SiteResponse;
import tn.sofrecom.servicetelecom.handlers.DeleteConfigurationException;
import tn.sofrecom.servicetelecom.handlers.ResourceNotFoundException;
import tn.sofrecom.servicetelecom.handlers.SiteRefServiceBussinessException;
import tn.sofrecom.servicetelecom.model.*;
import tn.sofrecom.servicetelecom.proxies.WebClientApi;
import tn.sofrecom.servicetelecom.repository.*;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class ServiceTelecomServiceImpl implements ServiceTelecomService{

    private final SiteFortinetRepository siteFortinetRepository;

    private final SiteL3VpnRepository siteL3VpnRepository;


    private final QosSInternetRepository qosSInternetRepository;

    private final L3VPNRepository l3VpnRepository;

    private final WebClientApi webClientApi ;
    private final InternetViaMPLSRepository internetViaMPLSRepository;
    private final ServiceTelecomRepository serviceTelecomRepository;



    public ServiceTelecomServiceImpl(SiteFortinetRepository siteFortinetRepository, SiteL3VpnRepository siteL3VpnRepository, QosSInternetRepository qosSInternetRepository, L3VPNRepository l3VpnRepository, WebClientApi webClientApi, InternetViaMPLSRepository internetViaMPLSRepository, ServiceTelecomRepository serviceTelecomRepository) {
        this.siteFortinetRepository = siteFortinetRepository;
        this.internetViaMPLSRepository = internetViaMPLSRepository;
        this.siteL3VpnRepository = siteL3VpnRepository;
        this.qosSInternetRepository = qosSInternetRepository;
        this.l3VpnRepository = l3VpnRepository;
        this.webClientApi = webClientApi;

        this.serviceTelecomRepository = serviceTelecomRepository;
    }
    @Override
    public List<ServiceTelecom> findAllRunningServicesBySiteId(Long siteId) {
        List<ServiceTelecom> runningServices = new ArrayList<>();

        siteFortinetRepository.findByIdSite(siteId).forEach(siteFortinet -> {
            FortinetServices service = siteFortinetRepository.findServiceByIdService(siteFortinet.getId().getIdService());
            if (service != null) {
                runningServices.add(service);
            }
        });
         Optional<InternetViaMPLS> internetViaMPLS = internetViaMPLSRepository.findInternetViaMPLSByIdSite(siteId);
         internetViaMPLS.ifPresent(runningServices::add);
        siteL3VpnRepository.findByIdSite(siteId).forEach(siteL3Vpn -> {
            L3VPN service = siteL3VpnRepository.findServiceByIdService(siteL3Vpn.getId().getIdService());
            if (service != null) {
                runningServices.add(service);
            }
        });
         Optional<QoSInternet> service = qosSInternetRepository.findQoSInternetByIdSite(siteId);
         service.ifPresent(runningServices::add);
        return runningServices;
    }
    public void deleteSiteL3VpnById(Long idSite, Long idService) {
        SiteL3VpnId siteL3VpnId = new SiteL3VpnId(idSite, idService);
        siteL3VpnRepository.deleteById(siteL3VpnId);
    }
    public void deleteL3VpnById(Long idService) {
        l3VpnRepository.deleteById(idService);
    }
    public Boolean deleteServiceL3VPN(L3VpnRequest l3VpnRequest){
        List<SiteResponse> siteResponses ;
        L3VpnResponsePushConfigurationDTO result;
        try{
        siteResponses = webClientApi.fetchSiteResponses(l3VpnRequest.getSiteIds());
        }
        catch (Exception exception){
            log.error("Exception occurred while fetch information site to database , Exception message {}", exception.getMessage());
            throw new SiteRefServiceBussinessException("Exception occurred while fetch a site");
        }
        L3VpnRequestPushConfigurationDTO l3VpnRequestPushConfigurationDTO = new L3VpnRequestPushConfigurationDTO();
        siteResponses.forEach(site -> {
            l3VpnRequestPushConfigurationDTO.getCity().add(site.getCity());
            l3VpnRequestPushConfigurationDTO.getSiteIds().add(site.getIdSite());
        });
        try{
            result = webClientApi.rollBackConfiguration(l3VpnRequestPushConfigurationDTO);
        }catch (Exception exception){
            log.error("Exception occurred while Delete Configuration , Exception message {}", exception.getMessage());
            throw new DeleteConfigurationException("Exception occurred while Delete Configuration");
        }
        result.getDevices().forEach((key, value) -> log.info("PE: {}, State: {}", key, value));
        try{
        siteResponses.forEach(site -> deleteSiteL3VpnById(site.getIdSite(), l3VpnRequest.getIdService()));
        deleteL3VpnById(l3VpnRequest.getIdService());}
        catch (Exception exception){
            log.error("Exception occurred while Delete VPN  , Exception message {}", exception.getMessage());
            throw new RuntimeException("Exception occurred while Delete VPN");
        }
            return true ;
        }


    @Override
    public void deleteInternetViaMplsById(Long idService) {
        internetViaMPLSRepository.deleteById(idService);

    }

    @Override
    public Boolean deleteServiceInternetViaMpls(InternetViaMplsRequest internetViaMplsRequest) {
        List<SiteResponse> siteResponses ;
        InternetViaMPLSResponseDTO result;
        try{
             siteResponses = webClientApi.fetchSiteResponses(internetViaMplsRequest.getSiteIds());
        } catch (Exception exception){
            log.error("Exception occurred while fetch information site to database , Exception message {}", exception.getMessage());
            throw new SiteRefServiceBussinessException("Exception occurred while fetch a site");
        }
        InternetViaMPLSRequestDTO internetViaMPLSRequestDTO = new InternetViaMPLSRequestDTO();
        internetViaMPLSRequestDTO.getCity().add(siteResponses.get(0).getCity());
        internetViaMPLSRequestDTO.getSiteIds().add(siteResponses.get(0).getIdSite());
        try {
             result = webClientApi.rollBackInternetConfiguration(internetViaMPLSRequestDTO);
        }catch (Exception exception){
            log.error("Exception occurred while Delete Configuration  , Exception message {}", exception.getMessage());
            throw new DeleteConfigurationException("Exception occurred while Delete Configuration ");
        }

            result.getDevices().forEach((key, value) -> log.info("CE: {}, State: {}", key, value));
        try{
            deleteInternetViaMplsById(internetViaMplsRequest.getIdService());
            Optional <QoSInternet> qosInternet = qosSInternetRepository.findQoSInternetByIdSite(siteResponses.get(0).getIdSite());
            qosSInternetRepository.deleteById(qosInternet.get().getIdService());
        }
        catch (Exception exception)
        {
            log.error("Exception occurred while Delete   , Exception message {}", exception.getMessage());
            throw new DeleteConfigurationException("Exception occurred while Delete  ");
        }
            return true ;

    }

    @Override
    public List<ServiceTelecom> findAllInternetAndQosService() {
        List<ServiceTelecom> runningServices = new ArrayList<>();
        List<QoSInternet> qoSInternet = qosSInternetRepository.findAll();
        runningServices.addAll(qoSInternet);

        return runningServices;

    }
    public List<ServiceResponse> getServices(String type, String serviceName) {
        List<ServiceTelecom> services = serviceTelecomRepository.findAll();
        List<ServiceResponse> serviceResponses = new ArrayList<>();
        services = services.stream()
                .filter(service -> type.equals(service.getClass().getSimpleName()))
                .collect(Collectors.toList());

        if (serviceName != null && !serviceName.isEmpty()) {
            services = services.stream()
                    .filter(service -> service instanceof FortinetServices
                            && serviceName.equals(((FortinetServices) service).getServiceName()))
                    .collect(Collectors.toList());
        }

        for (ServiceTelecom service : services) {
            ServiceResponse response = new ServiceResponse();
            response.setService(service);
            Set<SiteDTO> siteDTOS = new HashSet<>();

            if (service instanceof FortinetServices) {
                Set<Site_Fortinet> sites = ((FortinetServices) service).getSites();
                for (Site_Fortinet site : sites) {
                    siteDTOS.add(webClientApi.fetchSiteResponse(site.getId().getIdSite()));
                }
            } else if (service instanceof L3VPN) {
                Set<Site_L3Vpn> sites = ((L3VPN) service).getSites();
                for (Site_L3Vpn site : sites) {
                    siteDTOS.add(webClientApi.fetchSiteResponse(site.getId().getIdSite()));
                }
            }
            if (service instanceof QoSInternet) {
                Long idSite = ((QoSInternet) service).getIdSite();
                siteDTOS.add(webClientApi.fetchSiteResponse(idSite));

            } else if (service instanceof L3VPN) {
                Set<Site_L3Vpn> sites = ((L3VPN) service).getSites();
                for (Site_L3Vpn site : sites) {
                    siteDTOS.add(webClientApi.fetchSiteResponse(site.getId().getIdSite()));
                }
            }

            response.setSiteDTOS(siteDTOS);
            serviceResponses.add(response);
        }

        return serviceResponses;
    }
    public InternetResponse getInternetResponseBySiteId(Long siteId) {
        InternetViaMPLS internetService = internetViaMPLSRepository.findInternetViaMPLSByIdSite(siteId)
                .orElseThrow(() -> new ResourceNotFoundException("InternetViaMPLS not found with siteId: " + siteId));
        InternetResponse response = new InternetResponse();
        response.setIdServiceInternet(internetService.getIdService());
        return response;
    }


}
