package tn.sofrecom.servicetelecom.service;

import tn.sofrecom.servicetelecom.dto.ServiceResponse;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetResponse;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetViaMplsRequest;
import tn.sofrecom.servicetelecom.dto.l3VPN.L3VpnRequest;
import tn.sofrecom.servicetelecom.model.ServiceTelecom;

import java.util.List;

public interface ServiceTelecomService {
    List<ServiceTelecom> findAllRunningServicesBySiteId(Long siteId);
    void deleteSiteL3VpnById(Long idSite, Long idService) ;
    void deleteL3VpnById(Long idService) ;
    Boolean deleteServiceL3VPN(L3VpnRequest l3VpnRequest) ;

    void deleteInternetViaMplsById(Long idService) ;
    Boolean deleteServiceInternetViaMpls(InternetViaMplsRequest internetViaMplsRequest) ;

    List<ServiceTelecom> findAllInternetAndQosService();
    List<ServiceResponse> getServices(String type, String serviceName);
    InternetResponse getInternetResponseBySiteId(Long siteId);
}
