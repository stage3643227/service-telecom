package tn.sofrecom.servicetelecom.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import tn.sofrecom.servicetelecom.dto.useraccount.CreateUserRequest;
import tn.sofrecom.servicetelecom.model.FortinetServices;

import tn.sofrecom.servicetelecom.service.FortinetService;

@Slf4j
@RestController
@RequestMapping("/api/v1/FotinetService")
@CrossOrigin(origins = "http://localhost:4200")
public class FotinetServiceController {
    private final FortinetService fortinetService;

    public FotinetServiceController(FortinetService fortinetService) {
        this.fortinetService = fortinetService;
    }

    @PostMapping
    public FortinetServices createFortinetServiceWithUser(@RequestBody CreateUserRequest request) {

        return fortinetService.createFortinetServiceWithUser(request);
    }
}
