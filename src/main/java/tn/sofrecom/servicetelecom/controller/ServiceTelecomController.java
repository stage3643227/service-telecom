package tn.sofrecom.servicetelecom.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicetelecom.dto.APIResponse;
import tn.sofrecom.servicetelecom.dto.ServiceRequestDTO;
import tn.sofrecom.servicetelecom.dto.ServiceResponse;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetResponse;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetViaMplsRequest;
import tn.sofrecom.servicetelecom.dto.l3VPN.L3VpnRequest;
import tn.sofrecom.servicetelecom.factory.ServiceTelecomFactory;
import tn.sofrecom.servicetelecom.factory.ServiceTelecomFactoryProvider;
import tn.sofrecom.servicetelecom.model.ServiceTelecom;
import tn.sofrecom.servicetelecom.service.ServiceTelecomService;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequestMapping("/api/v1/ServiceTelecom")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ServiceTelecomController {
    public static final String SUCCESS = "Success";
    private ServiceTelecomFactoryProvider factoryProvider;
    private ServiceTelecomService serviceTelecomService;

    public ServiceTelecomController(ServiceTelecomFactoryProvider factoryProvider ,ServiceTelecomService serviceTelecomService ) {
        this.factoryProvider = factoryProvider;
        this.serviceTelecomService = serviceTelecomService;

    }
    @PostMapping("/createService")
    public ResponseEntity<APIResponse> createService(@Valid @RequestBody ServiceRequestDTO serviceRequestDTO) {
        log.info("ServiceTelecomController::createService Request Body service type  {} and site {}", serviceRequestDTO.getServiceType(), serviceRequestDTO.getSiteIds());
        ServiceTelecomFactory factory = factoryProvider.getFactory(serviceRequestDTO.getServiceType());
        ServiceTelecom service = factory.createService(serviceRequestDTO);
        APIResponse<ServiceTelecom> responseDTO =   APIResponse
                .<ServiceTelecom>builder()
                .status(SUCCESS)
                .results(service)
                .build();

        log.info("ServiceTelecomController::createService {}", service );
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }
    @GetMapping("/available")
    ResponseEntity<APIResponse> getAvailableServices() {
        log.info("ServiceTelecomController::getAvailableServices");
        List<String> availableServices = factoryProvider.getAvailableServiceTypes();
        APIResponse<List<String>> responseDTO =   APIResponse
                .<List<String>>builder()
                .status(SUCCESS)
                .results(availableServices)
                .build();

        log.info("ServiceTelecomController::getAvailableServices {}", availableServices );
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
    @GetMapping("runningServiceBySite/{siteId}")
    public List<ServiceTelecom> getRunningServicesBySiteId(@PathVariable Long siteId) {
        return serviceTelecomService.findAllRunningServicesBySiteId(siteId);
    }
    @PostMapping("deleteSiteL3VpnAndL3Vpn")
    public Boolean deleteServiceL3VPN(@Valid @RequestBody L3VpnRequest l3VpnRequest) {

        return serviceTelecomService.deleteServiceL3VPN(l3VpnRequest);
    }

    @PostMapping("deleteInternet")
    public Boolean deleteServiceInternet(@Valid @RequestBody InternetViaMplsRequest internetViaMplsRequest) {

        return serviceTelecomService.deleteServiceInternetViaMpls(internetViaMplsRequest);
    }

    @GetMapping("runningServiceInternetQos")
    public List<ServiceTelecom> getRunningServicesInternetQos() {
        return serviceTelecomService.findAllInternetAndQosService();
    }
    @GetMapping("getServices")
    public ResponseEntity<List<ServiceResponse>> getServices(@RequestParam String type, @RequestParam(required = false) String serviceName) {
        List<ServiceResponse> services = serviceTelecomService.getServices(type, serviceName);
        return ResponseEntity.ok(services);
    }
    @GetMapping("getInternetResponseBySiteId/{siteId}")
    public ResponseEntity<InternetResponse> getInternetResponseBySiteId(@PathVariable Long siteId) {
        InternetResponse services = serviceTelecomService.getInternetResponseBySiteId(siteId);
        return ResponseEntity.ok(services);
    }
}
