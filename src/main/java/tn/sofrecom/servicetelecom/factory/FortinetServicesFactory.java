package tn.sofrecom.servicetelecom.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import tn.sofrecom.servicetelecom.dto.ServiceRequestDTO;
import tn.sofrecom.servicetelecom.dto.site.SiteResponse;
import tn.sofrecom.servicetelecom.dto.user.UserResponse;
import tn.sofrecom.servicetelecom.handlers.SiteRefServiceBussinessException;
import tn.sofrecom.servicetelecom.model.*;
import tn.sofrecom.servicetelecom.proxies.WebClientApi;
import tn.sofrecom.servicetelecom.repository.FortinetServicesRepository;
import tn.sofrecom.servicetelecom.repository.SiteFortinetRepository;
import tn.sofrecom.servicetelecom.service.EmailService;

import java.time.LocalDateTime;
import java.util.List;

@Component
@Slf4j
public class FortinetServicesFactory implements ServiceTelecomFactory{
    private final EmailService emailService;
    private final WebClientApi webClientApi ;
    private final FortinetServicesRepository fortinetServicesRepository ;
    private final SiteFortinetRepository siteFortinetRepository ;
    public FortinetServicesFactory(EmailService emailService, WebClientApi webClientApi, FortinetServicesRepository fortinetServicesRepository, SiteFortinetRepository siteFortinetRepository) {
        this.emailService = emailService;
        this.webClientApi = webClientApi;
        this.fortinetServicesRepository = fortinetServicesRepository;
        this.siteFortinetRepository = siteFortinetRepository;
    }

    @Override
    public ServiceTelecom createService(ServiceRequestDTO serviceRequestDTO) {
        FortinetServices fortinetServices;
        UserResponse userResponse ;
        List<SiteResponse> siteResponses ;

        try{
         userResponse = webClientApi.fetchUserResponseByID(serviceRequestDTO.getIdUser());}
        catch (Exception exception){
            log.error("Exception occurred while fetch information user to database , Exception message {}", exception.getMessage());
            throw new SiteRefServiceBussinessException("Exception occurred while fetch a user");
        }
        try{
        siteResponses = webClientApi.fetchSiteByIdCompanyResponses(userResponse.getIdCompany());}
        catch (Exception exception){
            log.error("Exception occurred while fetch information site to database , Exception message {}", exception.getMessage());
            throw new SiteRefServiceBussinessException("Exception occurred while fetch a site");

        }
        fortinetServices = fortinetServicesRepository.findByServiceName(serviceRequestDTO.getServiceName());
        fortinetServices.setLicenseExpiration(LocalDateTime.now().plusMonths(6));
        siteResponses.forEach(siteResponse -> {
            Site_Fortinet siteFortinet = new Site_Fortinet();
            siteFortinet.setId(new SiteFortinetId(siteResponse.getIdSite(), fortinetServices.getIdService()));
            siteFortinetRepository.save(siteFortinet);
        });
        try {
            String to = userResponse.getEmail();
            String subject = "Votre nouveau service " + fortinetServices.getServiceName();
            String text = "Cher(e) " + userResponse.getFirstName() + " " + userResponse.getLastName() + ",\n\n" +
                    "Votre nouveau service "+ fortinetServices.getServiceName() +" a été créé avec succès. Voici les informations concernant votre nom d'utilisateur et votre mot de passe :\n\n" +
                    "Nom d'utilisateur : "+ fortinetServices.getUserAccount().getUsername()+" \n" +
                    "Mot de passe : "+fortinetServices.getUserAccount().getPassword()+"\n\n" +
                    "URL du service : " + fortinetServices.getUrl() + "\n\n" +
                    "Cordialement,\n\n" +
                    "L'équipe de support\n" ;
            emailService.sendSimpleMessage(to, subject, text);

        } catch (Exception exception){
            log.error("Exception occurred while send email , Exception message {}", exception.getMessage());
            throw new SiteRefServiceBussinessException("Exception occurred while send email");

        }


            return fortinetServices;

    }
}
