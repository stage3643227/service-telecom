package tn.sofrecom.servicetelecom.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import tn.sofrecom.servicetelecom.dto.ServiceRequestDTO;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetViaMPLSRequestDTO;
import tn.sofrecom.servicetelecom.dto.internetViaMpls.InternetViaMPLSResponseDTO;
import tn.sofrecom.servicetelecom.dto.site.SiteResponse;
import tn.sofrecom.servicetelecom.handlers.PushConfigurationException;
import tn.sofrecom.servicetelecom.handlers.SiteRefServiceBussinessException;
import tn.sofrecom.servicetelecom.handlers.TestConfigurationException;
import tn.sofrecom.servicetelecom.model.*;
import tn.sofrecom.servicetelecom.proxies.WebClientApi;
import tn.sofrecom.servicetelecom.repository.InternetViaMPLSRepository;
import tn.sofrecom.servicetelecom.repository.QosSInternetRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class InternetViaMPLSFactory implements ServiceTelecomFactory{
    private final WebClientApi webClientApi ;
    private final InternetViaMPLSRepository internetViaMPLSRepository;
    private final QosSInternetRepository qosSInternetRepository;
    private static final String RUNNING = "running";

    public InternetViaMPLSFactory(WebClientApi webClientApi, InternetViaMPLSRepository internetViaMPLSRepository, QosSInternetRepository qosSInternetRepository) {
        this.webClientApi = webClientApi;
        this.internetViaMPLSRepository = internetViaMPLSRepository;

        this.qosSInternetRepository = qosSInternetRepository;
    }

    @Override
    public ServiceTelecom createService(ServiceRequestDTO serviceRequestDTO) {
        List<SiteResponse> siteResponses ;
        InternetViaMPLSResponseDTO result ;
        Optional<InternetViaMPLS> existingInternetViaMPLS = internetViaMPLSRepository.findInternetViaMPLSByIdSite(serviceRequestDTO.getSiteIds().get(0));
        Optional<QoSInternet> existingQoSInternet = qosSInternetRepository.findQoSInternetByIdSite(serviceRequestDTO.getSiteIds().get(0));
        QoSInternet qoSInternet;
        InternetViaMPLS internetViaMPLS;
        if (existingInternetViaMPLS.isPresent()) {
            internetViaMPLS = existingInternetViaMPLS.get();
        } else {
            internetViaMPLS = new InternetViaMPLS();
        }
        if (existingQoSInternet.isPresent()) {
            qoSInternet = existingQoSInternet.get();
        }
        else {
            qoSInternet = new QoSInternet();
        }
        try{
        siteResponses = webClientApi.fetchSiteResponses(serviceRequestDTO.getSiteIds());}
        catch (Exception exception)
        {
            log.error("Exception occurred while fetch information site to database , Exception message {}", exception.getMessage());
            throw new SiteRefServiceBussinessException("Exception occurred while fetch a site");
        }

            InternetViaMPLSRequestDTO internetViaMPLSRequestDTO = new InternetViaMPLSRequestDTO();
            internetViaMPLSRequestDTO.getCity().add(siteResponses.get(0).getCity());
            internetViaMPLSRequestDTO.getSiteIds().add(siteResponses.get(0).getIdSite());
            System.out.println(serviceRequestDTO.getBandwidth());
            internetViaMPLSRequestDTO.setBandWidth(serviceRequestDTO.getBandwidth() );
            try {
             result = webClientApi.pushInternetViaMPLSConfiguration(internetViaMPLSRequestDTO);}
            catch (Exception exception){
                log.error("Push Configuration Error", exception.getMessage());
                throw new PushConfigurationException("Exception occurred while Push Configuration");
            }
            log.info("Configuration pushed successfully. Status: {}", result.getStatus());
            result.getDevices().forEach((key, value) -> log.info("CE: {}, State: {}", key, value));
            Integer testReturnCode = webClientApi.testInternetConfiguration(internetViaMPLSRequestDTO);
                if (testReturnCode != null && testReturnCode == 0) {
                    log.info("Test Return Code: {}", testReturnCode);
                    internetViaMPLS.setTypeService("InternetViaMPLS");
                    internetViaMPLS.setState(RUNNING);
                    internetViaMPLS.setIdSite(siteResponses.get(0).getIdSite());
                    internetViaMPLS = internetViaMPLSRepository.save(internetViaMPLS);
                    qoSInternet.setTypeService("QoSInternet");
                    qoSInternet.setBandWidth(serviceRequestDTO.getBandwidth());
                    qoSInternet.setIdSite(siteResponses.get(0).getIdSite());
                    qosSInternetRepository.save(qoSInternet);

                    return internetViaMPLS;

                }
                else throw new TestConfigurationException("Exception occurred while Test Configuration");


        }


}
