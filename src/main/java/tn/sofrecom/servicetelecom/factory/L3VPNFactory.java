package tn.sofrecom.servicetelecom.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import tn.sofrecom.servicetelecom.dto.ServiceRequestDTO;
import tn.sofrecom.servicetelecom.dto.l3VPN.L3VpnRequestPushConfigurationDTO;
import tn.sofrecom.servicetelecom.dto.l3VPN.L3VpnResponsePushConfigurationDTO;
import tn.sofrecom.servicetelecom.dto.site.SiteResponse;
import tn.sofrecom.servicetelecom.handlers.PushConfigurationException;
import tn.sofrecom.servicetelecom.handlers.SiteRefServiceBussinessException;
import tn.sofrecom.servicetelecom.handlers.TestConfigurationException;
import tn.sofrecom.servicetelecom.model.L3VPN;
import tn.sofrecom.servicetelecom.model.ServiceTelecom;
import tn.sofrecom.servicetelecom.model.SiteL3VpnId;
import tn.sofrecom.servicetelecom.model.Site_L3Vpn;
import tn.sofrecom.servicetelecom.proxies.WebClientApi;
import tn.sofrecom.servicetelecom.repository.L3VPNRepository;
import tn.sofrecom.servicetelecom.repository.SiteL3VpnRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class L3VPNFactory implements ServiceTelecomFactory  {
    private final WebClientApi webClientApi ;
    private final L3VPNRepository l3VPNRepository;
    private final SiteL3VpnRepository siteL3VpnRepository;
    private static final String RUNNING = "running";
    public L3VPNFactory( L3VPNRepository l3VPNRepository, SiteL3VpnRepository siteL3VpnRepository , WebClientApi webClientApi) {
        this.webClientApi = webClientApi;
        this.l3VPNRepository = l3VPNRepository;
        this.siteL3VpnRepository = siteL3VpnRepository;
    }

    @Override
    public ServiceTelecom createService(ServiceRequestDTO serviceRequestDTO)  {
        List<SiteResponse> siteResponses ;
        L3VpnResponsePushConfigurationDTO result ;
        Optional<L3VPN> existingL3Vpn = l3VPNRepository.findByTypeService("L3VPN");
        L3VPN l3Vpn;
        if (existingL3Vpn.isPresent()) {
            l3Vpn = existingL3Vpn.get();
        } else {
            l3Vpn = new L3VPN();
        }
        try{
             siteResponses = webClientApi.fetchSiteResponses(serviceRequestDTO.getSiteIds());
        }catch (Exception exception){
            log.error("Exception occurred while fetch information site to database , Exception message {}", exception.getMessage());
            throw new SiteRefServiceBussinessException("Exception occurred while fetch a site");
        }
        L3VpnRequestPushConfigurationDTO l3VpnRequestPushConfigurationDTO = new L3VpnRequestPushConfigurationDTO();
        siteResponses.forEach(site -> {
            l3VpnRequestPushConfigurationDTO.getCity().add(site.getCity());
            l3VpnRequestPushConfigurationDTO.getSiteIds().add(site.getIdSite());
        });
        try {
             result = webClientApi.pushConfiguration(l3VpnRequestPushConfigurationDTO);
        }
        catch (Exception exception){
            log.error("Push Configuration Error", exception.getMessage());
            throw new PushConfigurationException("Exception occurred while Push Configuration");
        }
        log.info("Configuration pushed successfully. Status: {}", result.getStatus());
        result.getDevices().forEach((key, value) -> log.info("PE: {}, State: {}", key, value));
        Integer testReturnCode = webClientApi.testConfiguration(l3VpnRequestPushConfigurationDTO);
        if (testReturnCode != null && testReturnCode == 0) {
                    log.info("Test Return Code: {}", testReturnCode);
                    l3Vpn.setTypeService("L3VPN");
                    l3Vpn.setState(RUNNING);
                    l3Vpn = l3VPNRepository.save(l3Vpn);
            L3VPN finalL3Vpn = l3Vpn;
            siteResponses.forEach(siteResponse -> {
                Site_L3Vpn siteL3Vpn = new Site_L3Vpn();
                siteL3Vpn.setId(new SiteL3VpnId(siteResponse.getIdSite(), finalL3Vpn.getIdService()));
                siteL3VpnRepository.save(siteL3Vpn);
            });

        }
        else throw new TestConfigurationException("Exception occurred while Test Configuration");

            return l3Vpn;

    }





}
