package tn.sofrecom.servicetelecom.factory;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ServiceTelecomFactoryProvider {

    private L3VPNFactory l3VPNFactory;

    private QoSInternetFactory qoSInternetFactory;

    private FortinetServicesFactory fortinetServicesFactory;

    private InternetViaMPLSFactory internetViaMPLSFactory;

    public ServiceTelecomFactoryProvider(L3VPNFactory l3VPNFactory, QoSInternetFactory qoSInternetFactory, FortinetServicesFactory fortinetServicesFactory, InternetViaMPLSFactory internetViaMPLSFactory) {
        this.l3VPNFactory = l3VPNFactory;
        this.qoSInternetFactory = qoSInternetFactory;
        this.fortinetServicesFactory = fortinetServicesFactory;
        this.internetViaMPLSFactory = internetViaMPLSFactory;
    }

    public ServiceTelecomFactory getFactory(String serviceType) {
        if (serviceType == null) {
            throw new IllegalArgumentException("Service type cannot be null");
        }
        switch (serviceType) {
            case "L3VPN":
                return l3VPNFactory;
            case "QoSInternet":
                return qoSInternetFactory;
            case "FortinetServices":
                return fortinetServicesFactory;
            case "InternetViaMPLS":
                return internetViaMPLSFactory;
            default:
                throw new IllegalArgumentException("Unknown service type: " + serviceType);
        }
    }
    public List<String> getAvailableServiceTypes() {
        List<String> availableServices = new ArrayList<>();

        availableServices.add("L3VPN");
        availableServices.add("QoSInternet");
        availableServices.add("appControl");
        availableServices.add("webfiltring");
        availableServices.add("ips");
        availableServices.add("InternetViaMPLS");

        return availableServices;
    }
}
