package tn.sofrecom.servicetelecom.factory;

import tn.sofrecom.servicetelecom.dto.ServiceRequestDTO;
import tn.sofrecom.servicetelecom.model.ServiceTelecom;


public interface ServiceTelecomFactory {
    ServiceTelecom createService(ServiceRequestDTO serviceRequestDTO);


}
