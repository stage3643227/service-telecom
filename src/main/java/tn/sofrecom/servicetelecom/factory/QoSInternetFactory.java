package tn.sofrecom.servicetelecom.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import tn.sofrecom.servicetelecom.dto.ServiceRequestDTO;
import tn.sofrecom.servicetelecom.dto.qosInternet.QosInternetPushConfigRequestDTO;
import tn.sofrecom.servicetelecom.dto.qosInternet.QosInternetPushConfigResponseDTO;
import tn.sofrecom.servicetelecom.dto.site.SiteResponse;
import tn.sofrecom.servicetelecom.handlers.PushConfigurationException;
import tn.sofrecom.servicetelecom.handlers.SiteRefServiceBussinessException;
import tn.sofrecom.servicetelecom.model.*;
import tn.sofrecom.servicetelecom.proxies.WebClientApi;
import tn.sofrecom.servicetelecom.repository.QosSInternetRepository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class QoSInternetFactory implements ServiceTelecomFactory{
    private final WebClientApi webClientApi ;
    private final QosSInternetRepository qosSInternetRepository;


    public QoSInternetFactory(WebClientApi webClientApi, QosSInternetRepository qosSInternetRepository) {
        this.webClientApi = webClientApi;
        this.qosSInternetRepository = qosSInternetRepository;

    }

    @Override
    public ServiceTelecom createService(ServiceRequestDTO serviceRequestDTO) {
        Optional<QoSInternet> existingQoSInternet = qosSInternetRepository.findQoSInternetByIdSite(serviceRequestDTO.getSiteIds().get(0));
        QoSInternet qoSInternet;
        QosInternetPushConfigResponseDTO result ;
        List<SiteResponse> siteResponses ;
        if (existingQoSInternet.isPresent()) {
                qoSInternet = existingQoSInternet.get();
        }
        else {
            qoSInternet = new QoSInternet();
        }
        try {
             siteResponses = webClientApi.fetchSiteResponses(serviceRequestDTO.getSiteIds());
        }
        catch (Exception exception){
            log.error("Exception occurred while fetch information site to database , Exception message {}", exception.getMessage());
            throw new SiteRefServiceBussinessException("Exception occurred while fetch a site");
        }
        QosInternetPushConfigRequestDTO qosInternetPushConfigRequestDTO = new QosInternetPushConfigRequestDTO();
        qosInternetPushConfigRequestDTO.getCity().add(siteResponses.get(0).getCity());
        qosInternetPushConfigRequestDTO.getSiteIds().add(siteResponses.get(0).getIdSite());
        qosInternetPushConfigRequestDTO.setBandWidth(serviceRequestDTO.getBandwidth());
        log.info("qosInternetPushConfigRequestDTO", qosInternetPushConfigRequestDTO.toString());
        try {
             result = webClientApi.pushConfigurationQos(qosInternetPushConfigRequestDTO);
        }
        catch (Exception exception) {
            log.error("Push Configuration Error", exception.getMessage());
            throw new PushConfigurationException("Exception occurred while Push Configuration");
        }
        log.info("Configuration pushed successfully. Status: {}", result.getStatus());
        result.getDevices().forEach((key, value) -> log.info("PE: {}, State: {}", key, value));
        qoSInternet.setTypeService("QoSInternet");
        qoSInternet.setBandWidth(serviceRequestDTO.getBandwidth());
        qoSInternet.setIdSite(siteResponses.get(0).getIdSite());
        qoSInternet = qosSInternetRepository.save(qoSInternet);
        return qoSInternet ;
        }

}
